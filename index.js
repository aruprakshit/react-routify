import { render } from "react-dom";
import React from "react";
import Home from './components/Home';

require("./stylesheets/main.scss");


const containerEl = document.getElementById("container");

render(
  <Home/>,
  containerEl
);
