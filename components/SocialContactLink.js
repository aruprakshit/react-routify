import React, { Component } from "react";

const SocialContactLink = (props) => {
  return (
    <p>
      <a href={props.url}>
        <img src={props.src} alt=""/>
         {props.content}
      </a>
    </p>
  )
}

export default SocialContactLink;
