import React, { Component } from "react";
import { Grid, Row, Col } from 'react-bootstrap';
import SocialContactLink from './SocialContactLink';

const About = () => {
  return (
    <Grid>
      <Row>
        <Col md={12}>
          <span className='h1'>About</span>
          <hr/>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          <p>If you have a question about a Stack Exchange site, the best place to ask is on Meta Stack Exchange or the sites own meta. (Look under "help" on the top bar.)</p>
          <p> You might find these links useful: </p>
        </Col>
      </Row>
      <Row>
        <Col md={12}>
          <SocialContactLink
            url="https://www.facebook.com/help/"
            src="http://i.stack.imgur.com/9w2PY.png?s=32"
            content=' Facebook Help Center'/>
        </Col>
        <Col md={12}>
          <SocialContactLink
            url="https://help.yahoo.com/kb/helpcentral"
            src="http://i.stack.imgur.com/XxJ5f.jpg?s=32"
            content=' Yahoo! Help Central'/>
        </Col>
        <Col md={12}>
          <SocialContactLink
            url="http://www.google.com/contact/"
            src="http://i.stack.imgur.com/pJgkU.png?s=32"
            content=' Google Contact'/>
        </Col>
      </Row>
    </Grid>
  )
}

export default About;
