import React, { Component } from "react";
import About from './About';
import { Router, Link } from 'react-routify';
import { Grid, Row, ListGroup, ListGroupItem } from 'react-bootstrap';

const routes = [
  { path: '/', component: Home },
  { path: '/about', component: About }
]

const Home = () => {
  return (
    <Grid fluid={true}>
        <Row>
          <ListGroup>
            <ListGroupItem href="/" className="text-center" active='true'>Home</ListGroupItem>
            <ListGroupItem className="text-center" href='/about'>About</ListGroupItem>
          </ListGroup>
       </Row>
       <Row>
         <Router routes={routes} />
      </Row>
    </Grid>
  )
}

export default Home;
